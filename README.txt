SquaredHDIndicator
------------------------

This plugin is based on the popular plugin squaredhotindicators. I was inspired  for this plugin by nevor who 
is also a healer and We both struggle with heal debuffs . This plugin does not remove any heal debuffs it's just
a help for healers to see if a heal debuff is on a target.

The indicators are pink for a weak debuff (25%), black (50%) and orange the outgoing heal debuff at the left bottom of each of the squared boxes
in your group. Because of the mechanics i used im not able to show the debuffs for the whole warband at the
moment. 

If you experience that the plugin does not show a specific debuff please send me an email with the name of the
ability and which class it can cast and will add it.

best regards
Majeric - Warrior Priest - Naeramarth/Erengrad
maje@chello.at