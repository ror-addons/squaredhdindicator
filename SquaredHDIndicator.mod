<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="SquaredHDIndicator" version="1.2" date="27/04/2009" >
		
		<Author name="majeric" email="majestic@naeramarth.com" />
		<Description text="heal debuff indicator plugin for Squared." />

		<VersionSettings gameVersion="1.3.4" windowsVersion="1.0" savedVariablesVersion="1.0" />
        
		<Dependencies>
		  <Dependency name="SquaredSettings" />
		</Dependencies>
		<Files>
		  <File name="SquaredHDIndicator.lua" />
		</Files>
	
		<OnInitialize>
		  <CallFunction name="HDI.OnInitialize" />
		</OnInitialize>
		<OnUpdate>
		  <CallFunction name="HDI.OnUpdate" />
		</OnUpdate>		
	</UiMod>
</ModuleFile>
