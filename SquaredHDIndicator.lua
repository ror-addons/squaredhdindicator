HDI = {}

local SHDIVersion = L"1.1"
local TimePassed = 0
local RenderFreq = 0.5
local RenderData = {}
local PlayerName = L""
local PlayerGroup = 0
local UnitCache = {} -- This will be filled up by the squared Addon so we dont have to do much here
local HDTable25 = {}
local HDTable50 = {}
local HDTableOut = {}
local HDTableDestruction50 = {
-- Chaos Heal Debuffs
   -- Zealot
   [8613] = 50, -- Windblock 50%
   -- Disciple Of Khaine
   [9602] = 50, -- Curse Of Khaine 50%
   [3428] = 50, -- Curse Of Khaine 50%
   -- Witch Elf
   [9424] = 50, -- Black Lotus Blade 50%
   -- Marauder
   [8440] = 50, -- Deadly Clutch 50%
   -- Squig Herder
   [1853] = 50, -- Rotten Arrer
   -- Choppa
   [1773] = 50, -- No More Helpin
   [1803] = 50, -- Yer Goin Down
   -- Shaman
   [1905] = 50, -- Gork's Barbs
   [3352] = 50, -- Gork's Barbs
}
local HDTableOrder50 = {
-- Order Heal Debuffs
   -- Witch Hunter
   [8112] = 50, -- Punish The False 50 %
   [20324] = 50, -- Punish The False 50 %
   -- Shadowwarrior
   [9109] = 50, -- Shadow Sting 50%
   -- Slayer
   [1434] = 50, -- Deep Wound 50%
   [12692] = 50, -- Deep Wound 50%
   [1501] = 50, -- Looks like a Challenge 50%
   -- White Lion
   [9191] = 50, -- Thin the Herd 50%
}
local HDTableDestruction25 = {
   -- Chosen
   [8348] = 25, -- Discordant Turbulence 25%
   [3409] = 25, -- Discordant Turbulence 25%
   [3776] = 25, -- Discordant Turbulence 25%
   -- Marauder
   [8401] = 25, -- Tainted Claw 25%
   -- Witch Elf
   [10412] = 25 -- Guile 25%
}
local HDTableOrder25 = {
    -- Knight of the blazing sun
   [8036] = 25, -- Now's Our Chance! 25%
   [3493] = 25, -- Now's Our Chance! 25%
}
local HDTableOrderOut = {
	-- Witch Hunter
	[3002] = 50, -- Blessed Bullets of Confession
	[3784] = 50, -- Blessed Bullets of Confession
	[8099] = 50, -- Blessed Bullets of Confession
}
local HDTableDestructionOut = {
	-- With Elf
	[3811] = 50, -- Kiss of Death
	[3598] = 50, -- Kiss of Death
	[9407] = 50, -- Kiss of Death
	-- Blackguard
	[9317] = 50, -- Mind Killer
}
local GetBuffs = GetBuffs

-- as it seems... debuffs like auras dont come fully compliant in events 
-- from the event system so lets just use the WAR API functions for debuff information
function HDI.OnUpdate(elapsed)    
    TimePassed = TimePassed + elapsed
    if (TimePassed >= RenderFreq) then
    	TimePassed = 0
    	HDI.UpdateHealDebuffs()
    end
end


function HDI.OnInitialize()
  -- dont want to use the eventsystem at the moment
  -- RegisterEventHandler(SystemData.Events.PLAYER_TARGET_EFFECTS_UPDATED, "HDI.EffectsUpdated")
  -- RegisterEventHandler(SystemData.Events.PLAYER_EFFECTS_UPDATED, "HDI.EffectsUpdated")
  -- RegisterEventHandler(SystemData.Events.GROUP_EFFECTS_UPDATED, "HDI.EffectsUpdated")
  RegisterEventHandler(SystemData.Events.LOADING_END, "HDI.LoadingEnd")
  -- RegisterEventHandler(SystemData.Events.RELOAD_INTERFACE, "HDI.LoadingEnd")
  
  Squared.RegisterEventHandler("cleargroups", HDI.ClearGroupHandler)
  Squared.RegisterEventHandler("setname", HDI.SetNameHandler)
  
  playerName = (GameData.Player.name):match(L"([^^]+)")
  
  Squared.SetSetting("hd-color1",Squared.GetSetting("hd-color1") or {190,0,128})
  Squared.SetSetting("hd-color2",Squared.GetSetting("hd-color2") or {0,0,0})
  Squared.SetSetting("hd-color3",Squared.GetSetting("hd-color3") or {255,128,64})
  -- Squared.SetSetting("hd-color4",Squared.GetSetting("hd-color4") or {255,255,255}) for future use
  
  PlayerName = (GameData.Player.name):match(L"([^^]+)")
  
  if (GameData.Player.realm == GameData.Realm.ORDER) then
  	 for i,k in pairs(HDTableDestruction50) do
  	 	HDTable50[i] = k
  	 end
  	 for i,k in pairs(HDTableDestruction25) do
  	 	HDTable25[i] = k
  	 end
  	 for i,k in pairs(HDTableDestructionOut) do
  	 	HDTableOut[i] = k
  	 end
  else
  	 for i,k in pairs(HDTableOrder50) do
  	 	HDTable50[i] = k
  	 end
  	 for i,k in pairs(HDTableOrder25) do
  	 	HDTable25[i] = k
  	 end
  	 for i,k in pairs(HDTableOrderOut) do
  	 	HDTableOut[i] = k
  	 end
  end
end

function HDI.LoadingEnd()
	EA_ChatWindow.Print(L"SquaredHDIndicator "..SHDIVersion..L" loaded")
end

function HDI.NewAbilityLearned()
end

-- this part should be working
function HDI.ClearGroupHandler()
  UnitCache = {}
  savedRenderFreq = tonumber(Squared.GetSetting("bufftracking-render-freq"))
  if (savedRenderFreq or 0.0) < 0.5 then
  	RenderFreq = 0.5
  else
  	RenderFreq = savedRenderFreq
  end
  RenderData.size = tonumber(Squared.GetSetting("status-size")) / InterfaceCore.GetScale()
  
  local aframe = Squared.GetSetting("status-hd")

   -- hopefully fixed position in the bottom left corner of the squared boxes
   RenderData.point = "bottomright"
   RenderData.relpoint = "bottomleft"
   RenderData.relframe = "BotLeft"
end


function HDI.SetNameHandler(unit)
  if (unit.name == PlayerName) then
  	PlayerGroup = unit.group
  end
  if (unit.group == PlayerGroup) then
  	local wname = "SquaredUnit_"..unit.group.."_"..unit.member
  	UnitCache[unit.name] = unit
  	-- hide the default SBI-Thingie
  	WindowSetAlpha(wname..RenderData.relframe,0)

	for i=1,3 do
		local fname = wname.."_HD_"..i
		if (not DoesWindowExist(fname)) then
			CreateWindowFromTemplate(fname, "EA_FullResizeImage_TintableSolidBackground", wname)
			WindowSetShowing(fname, false)
			WindowClearAnchors(fname)
			WindowSetDimensions(fname, RenderData.size, RenderData.size)
			WindowSetLayer(fname,Window.Layers.OVERLAY)
			if (i == 1) then
				WindowAddAnchor(fname, RenderData.point, wname..RenderData.relframe, RenderData.point, 0, 0)
				WindowSetTintColor(fname, unpack(Squared.GetSetting("hd-color"..1)))
			else
				WindowAddAnchor(fname, RenderData.point, wname.."_HD_"..(i-1), RenderData.relpoint, 0, 0)
				WindowSetTintColor(fname, unpack(Squared.GetSetting("hd-color"..i)))
			end
		end
	end
  end
end
-- end of part which i dont touch at the moment


-- if a player receives or looses effects we get an update ... hopefully
function HDI.EffectsUpdated(updateType, effects)
end

-- handleHealDebuffs: checks buffs for player and group. shows heal debuffs
--  if necessary.
function HDI.UpdateHealDebuffs()
 	local FrameName = nil
	local BuffList = nil
 	local HealDebuff1 = false
 	local HealDebuff2 = false
 	local HealDebuff3 = false
 	BuffList = GetBuffs(GameData.BuffTargetType.SELF)
    FrameName = "SquaredUnit_"..UnitCache[PlayerName].group.."_"..UnitCache[PlayerName].member.."_HD_"
    HealDebuff1 = HDI.hasHealDebuffFromTable(BuffList,HDTable25)
    HealDebuff2 = HDI.hasHealDebuffFromTable(BuffList,HDTable50)
    HealDebuff3 = HDI.hasHealDebuffFromTable(BuffList,HDTableOut)
 	HDI.showHealDebuffsForFrame(HealDebuff1,HealDebuff2,HealDebuff3,FrameName)
 	for index,member in ipairs(GroupWindow.groupData) do
		local unit = UnitCache[(member.name):match(L"([^^]+)")]
		if (unit) then
			local groupIndex = index - 1
			BuffList = GetBuffs(groupIndex)
    		FrameName = "SquaredUnit_"..unit.group.."_"..unit.member.."_HD_"
    		HealDebuff1 = HDI.hasHealDebuffFromTable(BuffList,HDTable25)
    		HealDebuff2 = HDI.hasHealDebuffFromTable(BuffList,HDTable50)
    		HealDebuff3 = HDI.hasHealDebuffFromTable(BuffList,HDTableOut)
 			HDI.showHealDebuffsForFrame(HealDebuff1,HealDebuff2,HealDebuff3,FrameName)
		end
	end
 end
 
 -- showHealDebuffsForFrame
 -- param [in] HealDebuff1 = first healdebuff to show
 -- param [in] HealDebuff2 = second healdebuff to show
 -- param [in] HealDebuff3 = third healdebuff to show
 -- param [in] FrameName = the unit frame we want to display
function HDI.showHealDebuffsForFrame(HealDebuff1,HealDebuff2,HealDebuff3,FrameName)
 	 if (HealDebuff1) then
      		WindowSetShowing(FrameName..1, true)
    	else
    		WindowSetShowing(FrameName..1, false)
    	end
    	if (HealDebuff2) then
    		WindowSetShowing(FrameName..2, true)
    	else
    		WindowSetShowing(FrameName..2, false)
    	end
    	if (HealDebuff3) then
    		WindowSetShowing(FrameName..3, true)
    	else
    		WindowSetShowing(FrameName..3, false)
    	end
 end
 
 -- hasHealDebuffWithStrength: looks for debuffs of the given table
 -- param [in] DebuffList = current list of buffs
 -- param [in] DebuffTable = table to compare the list against
 function HDI.hasHealDebuffFromTable(DebuffList,DebuffTable)
	if (DebuffList) then
 		for index,BuffData in pairs(DebuffList) do
    			--if BuffData.isDebuff then  -- does not look like all debuffs are debuffs
    				if (DebuffTable[BuffData.abilityId]) then
    					return true
    				end
    			--end
    		end
	end
    return false
 end